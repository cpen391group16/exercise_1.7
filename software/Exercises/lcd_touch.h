/*
 * lacd_touch.h
 *
 *  Created on: Jan 25, 2018
 *      Author: Mathew
 */

#ifndef LCD_TOUCH_H_
#define LCD_TOUCH_H_

typedef struct {int x, y; } Point;

void Init_Touch(void);

int ScreenTouched(void);
int ScreenPressed(void);
int ScreenReleased(void);

void WaitForTouch(void);
void WaitForPress(void);
void WaitForRelease(void);

Point GetPress(void);
Point GetRelease(void);

#endif /* LCD_TOUCH_H_ */
