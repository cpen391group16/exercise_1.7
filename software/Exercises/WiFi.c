/*
 * WiFi.c

 *
 *  Created on: Jan 23, 2018
 *      Author: timot
 */

#include "WiFi.h"

#define WIFI_Control (*(volatile unsigned char *)(0x84000240))
#define WIFI_Status (*(volatile unsigned char *)(0x84000240))
#define WIFI_TxData (*(volatile unsigned char *)(0x84000242))
#define WIFI_RxData (*(volatile unsigned char *)(0x84000242))
#define WIFI_Baud (*(volatile unsigned char *)(0x84000244))

void init_wifi(void)
{

	// set up 6850 Control Register to utilise a divide by 16 clock,
	// set RTS low, use 8 bits of data, no parity, 1 stop bit,
	// transmitter interrupt disabled
	WIFI_Control = 0x15;

	// program baud rate generator to use 115k baud
	WIFI_Baud = 0x01;

}

int sendchar_wifi(int c)
{
	// Poll Tx bit in 6850 status register and await for it to become '1'
	while (!(0x02 & WIFI_Status)) {};

	// Write the character to the 6850 TxData register.
	WIFI_TxData = c;

	return c;
}
int getchar_wifi( void )
{
	// Poll RX bit in 6850 status register and await for it to become '1'
	while (!(0x01 & WIFI_Status)) {};

	// Read the received character from 6850 RxData register.
	return (int) WIFI_RxData;
}

// the following function polls the 6850 to determine if any character
// has been received. It doesn't wait for one, or read it, it simply tests
// to see if one is available to read
int is_data_ready_wifi(void)
{
	// Test Rx bit in 6850 serial comms chip status register
	// if RX bit is set, return TRUE, otherwise return FALSE
	return 0x01 & WIFI_Status;
}

void send_test_text_message()
{
	sendstring_wifi("dofile(\"send_text_message.lua\")");
	sendstring_wifi("check_wifi()");
}

void sendstring_wifi(char* message){
	int i = 0;
	while(message[i] != '\0'){
		sendchar_wifi(message[i++]);
	}
	sendchar_wifi('\r');
}
