/*
 * WiFi.h
 *
 *  Created on: Jan 23, 2018
 *      Author: timot
 */

#ifndef WIFI_H_
#define WIFI_H_

void init_wifi(void);
int sendchar_wifi(int c);
void sendstring_wifi();

int getchar_wifi( void );
int is_data_ready_wifi(void);



#endif /* WIFI_H_ */
