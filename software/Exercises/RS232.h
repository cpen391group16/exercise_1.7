/*
 * RS232.h
 *
 *  Created on: Jan 20, 2018
 *      Author: timot
 */

#ifndef RS232_H_
#define RS232_H_

/**************************************************************************
/* Subroutine to initialise the RS232 Port by writing some data
** to the internal registers.
** Call this function at the start of the program before you attempt
** to read or write to data via the RS232 port
**
** Refer to 6850 data sheet for details of registers and
***************************************************************************/

void Init_RS232(void);
int sendcharRS232(int c);
int getcharRS232( void );
int RS232TestForReceivedData(void);

#endif /* RS232_H_ */
