-- This information is used by the Wi-Fi dongle to make a wireless connection to the router in the Lab
-- or if you are using another router e.g. at home, change ID and Password appropriately
SSID = "CPEN391"
SSID_PASSWORD = "group16wifi"

-- configure ESP as a station
wifi.setmode(wifi.STATION)
wifi.sta.config(SSID,SSID_PASSWORD)
wifi.sta.autoconnect(1)

function check_wifi()
    ip = wifi.sta.getip()
  
   if(ip==nil) then
     print("Connecting...")
   else
    tmr.stop(0)
    print("Connected to AP!")
    print(ip)
   end
end

-- uart.setup(0, 115200, 8, uart.PARITY_NONE, uart.STOPBITS_1, 0)
uart.on("data", "\r",
  function(data)
    --print("receive from uart:", data)
    --check_wifi()
    if data=="quit\r" then
        uart.on("data") -- unregister callback function
    elseif data =="get_tix\r" then
       --print("Getting Tickets")
        http.get("http://128.189.199.103:3001/tickets/1/"
        , nil, function(code, data)
            if (code < 0) then
                --print("HTTP request failed")
            else
                uart.write(0,data)
                uart.write(0,'\r')
            end
        end)
    end
end, 0)

